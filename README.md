# Teste API (youtube)


- Problema: Criar uma página que exiba os 9 resultados mais relevantes de um termo buscado, no youtube.


1 ) Primeiro ponto como consumir uma API;

2 ) Segundo ponto devolver os resultados de forma dinâmica, usando bootstrap e com o menor numero possível de linhas.


- Resolução:

Aprender como utilizar uma das API do site citado no teste, neste caso utilizei a documentaçao do google e tutoriais.

Com base num exemplo de script busca em JS, ele lista os arquivos dentro do elemento pre estabelecido no arquivo item.html

Foram feitos ajustes para alinhar o ultimo bloco de classe (.item) no CSS, o mesmo não consegui fazer o addClass() do Jquery funcionar nele.

Criei um reset CSS para que os videos dentro dos iframes não distorçam se a tela sofrer resize.

Usei o mínimo de CSS possível para customizações, e como pode perceber não tenho muito conhecimento no JS "avançado".
