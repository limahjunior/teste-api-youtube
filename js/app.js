function tplrender(e, t) {
    res = e;
    for (var n = 0; n < t.length; n++) {
        res = res.replace(/\{\{(.*?)\}\}/g, function (e, r) {
            return t[n][r]
        })
    }
    return res;
}

$(function () {
    $("form").on("submit", function (e) {
        e.preventDefault();
        // prepare the request
        var request = gapi.client.youtube.search.list({
            part: "snippet",
            type: "video",
            q: encodeURIComponent($("#search").val()).replace(/%20/g, "+"),
            maxResults: 9,
            order: "viewCount",
            publishedAfter: "2015-01-01T00:00:00Z"
        });
        // execute the request
        request.execute(function (response) {
            var results = response.result;
            $("#results").html("");
            $.each(results.items, function (index, item) {
                $.get("tpl/item.html", function (data) {
                    $(".item").addClass("col-md-4");
                    $("#results").append(tplrender(data, [{"title": item.snippet.title, "videoid": item.id.videoId}]));
                });
            });
        });
    });
});

function init() {
    gapi.client.setApiKey("AIzaSyDgGbHqm7SslzoVo_QgpW8pkJuR6tJb8Fw");
    gapi.client.load("youtube", "v3", function () {
        // yt api is ready
    });
}